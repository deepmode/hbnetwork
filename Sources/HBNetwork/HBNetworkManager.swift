//
//  HBNetworkManager.swift
//  SwiftDemo
//
//  Created by Eric Ho on 18/11/2020.
//

import Foundation
import Alamofire

public enum NetworkError: Error {
    case errorWithDetail(String?)
    case dataNilError
    
    public var detail:String? {
        switch self {
        case .dataNilError: return nil
        case .errorWithDetail(let t): return t
        }
    }
    
    public var createError:Error {
        switch self  {
        case .dataNilError:
            return self.createError(msg: "Data nil error")
        case .errorWithDetail(let s):
            return self.createError(msg: s ?? "")
        }
    }
    
    public func createError(msg:String) -> Error {
        return NSError(domain: "App", code: 900, userInfo: [NSLocalizedDescriptionKey : msg]) as Error
    }
}

public struct HBNetworkManager<ModelType:Decodable> {
    
    private let httpHeaders:HTTPHeaders
    
//    private var httpHeaders:HTTPHeaders = {
//
//        let e = HBAppConstants.requestHeaders(deviceToken: "", excludeAuthorization: false, bearerToken: nil)
//        var h = HTTPHeaders()
//        for (name,value) in e {
//            h.add(name: name, value: value)
//        }
//        return h
//
////        var h = HTTPHeaders()
////        h.add(HTTPHeader(name: "Content-Type", value: "application/json"))
////        h.add(HTTPHeader(name: "Accept", value: "application/json"))
////        h.add(HTTPHeader(name: "X-Api-Version", value: "2.13"))
////        return h
//    }()
    
    fileprivate let alamofireSessionManagerNoCache:Session
    public typealias HeaderKey = String
    public typealias HeaderValue = String
    
    public init(timeout:TimeInterval, ignoreCache:Bool, defaultHTTPHeaders:[HeaderKey:HeaderValue]) {
        
        var h = HTTPHeaders()
        for (name,value) in defaultHTTPHeaders {
            h.add(name: name, value: value)
        }
        self.httpHeaders = h
        
        //-----
        //For default sessions, the default value is the shared URL cache object. e.g. URLCache.shared
        let configuration = URLSessionConfiguration.default
        //configuration.urlCredentialStorage = nil
        
        //#########################
        //Conapple-reference-documentation://hshGyThy1yfgure cache policy
        //https://stackoverflow.com/questions/32199494/how-to-disable-caching-in-alamofire
        if ignoreCache {
            configuration.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData  //note: from apple document "This constant is unimplemented and shouldn’t be used"
            configuration.urlCache = nil //disable cache
        }
        //#########################
        
        /*
        //Test
        let memoryCapacity = 100 * 1024 * 1024; // 100 MB
        let diskCapacity = 100 * 1024 * 1024; // 100 MB
        configuration.urlCache = URLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "app_cache")
        configuration.requestCachePolicy = .useProtocolCachePolicy
        */
        
        //Allow cookies if needed.
        //configuration.httpCookieStorage = HTTPCookieStorage.shared
                
        configuration.timeoutIntervalForRequest = timeout // seconds
        let alamofireManager = Session(configuration: configuration)
        
        self.alamofireSessionManagerNoCache = alamofireManager
        //-----
    }
    
    
    public func fetchData(urlLink:String, method: HTTPMethod, params:[String:String]?, headers:[String:String]?, completion: @escaping (Result<ModelType,NetworkError>) -> Void) {
        
        var requestHeaders:HTTPHeaders = self.httpHeaders
        
        if let inputHeaders = headers {
            var h = HTTPHeaders()
            for (name,value) in inputHeaders {
                h.add(name: name, value: value)
            }
            requestHeaders = h
        }
        
        //https://www.hackingwithswift.com/articles/161/how-to-use-result-in-swift
        alamofireSessionManagerNoCache.request(urlLink, method: method, parameters: params, encoder: URLEncodedFormParameterEncoder.default, headers: requestHeaders, interceptor: nil)
            //.authenticate(with: self.credential)
            .validate(statusCode: 200..<300)
            //.validate(contentType: ["application/json"])
            .cURLDescription { description in
                print(description)
            }
            .responseJSON { (response) in
                
                debugPrint("Note: display debug response by uncommenting the #DebugPrintFetchDataResponse")
                //#DebugPrintFetchDataResponse
                //debugPrint(response)
                
                switch response.result {
                    case .failure(let error):
                        return completion(.failure(.errorWithDetail(error.localizedDescription)))
                    case .success(_):
                        
                        guard let decodeData = response.data else {
                            return completion(.failure(.dataNilError))
                        }
                        
                        func errorDescriptionHelper(context:DecodingError.Context) -> String? {
                            /*
                            context.codingPath
                            context.debugDescription
                            context.underlyingError
                            */
                            return "\(context.debugDescription) -> \(context.codingPath.debugDescription)"
                        }
                        
                        
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .iso8601
                        do {
                            let modelTypeResults = try JSONDecoder().decode(ModelType.self, from: decodeData)
                            return completion(.success(modelTypeResults))
                        } catch DecodingError.keyNotFound(let key, let context) {
                            print("keyNotFound: \(key)")
                            return completion(.failure(.errorWithDetail(errorDescriptionHelper(context: context))))
                        } catch DecodingError.valueNotFound(let key, let context) {
                            print("valueNotFound: \(key)")
                            return completion(.failure(.errorWithDetail(errorDescriptionHelper(context: context))))
                        } catch DecodingError.typeMismatch(let key, let context) {
                            print("typeMismatch: \(key)")
                            return completion(.failure(.errorWithDetail(errorDescriptionHelper(context: context))))
                            
                        } catch {
                            print("----- DecodingError: other error -----")
                            print(error.localizedDescription)
                            return completion(.failure(.errorWithDetail(error.localizedDescription)))
                        }
                }
        }
    }
}


