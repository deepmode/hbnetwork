import XCTest

import HBNetworkTests

var tests = [XCTestCaseEntry]()
tests += HBNetworkTests.allTests()
XCTMain(tests)
